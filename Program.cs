﻿using System;

namespace p1
{
    class Program
    {
        public decimal scholarshipCalculator(int semNum, double percentage, char prereq) {
            decimal totalScholarship = 0.00m;
            switch (semNum) {
                case 1 :
                    if(percentage > 75) {
                        totalScholarship = 2000.00m;
                    } else if(percentage >= 70 && percentage <= 75){
                        totalScholarship = 1000.00m;
                    } else {
                        totalScholarship = 0.00m;
                    }
                    break;
                case 2 :
                case 3 :
                    totalScholarship = 2500.00m;
                    break;
                case 4 :
                    if (prereq == 'y') {
                        totalScholarship = 2500.00m;
                    } else {
                        totalScholarship = 1300.00m;
                    }
                    break;
            }
            return totalScholarship;
        }

        static void Main(string[] args)
        {
            Program p = new Program();
            Console.WriteLine("Welcome to Financial Assistance of NWMSU");
            Console.WriteLine("\nEnter your name");
            String name = Console.ReadLine();
            double GPA = 3.33;
            bool counter = true;
            do {
                Console.WriteLine("\nEnter the semester number you want to check the scholarship details");
                int semNum = Convert.ToInt32(Console.ReadLine());
                double percentage = 0.0;
                if (semNum == 1){
                    Console.WriteLine("\nEnter the undergraduation percentage");
                    percentage = Convert.ToDouble(Console.ReadLine());
                }
                char prereq = 'y';
                if (semNum == 4) {
                    Console.WriteLine("\nDo you have prerequisites? (y/n)");
                    prereq = Console.ReadKey().KeyChar;
                    Console.ReadLine();
                }
                decimal totalScholarShip = p.scholarshipCalculator(semNum, percentage, prereq);
                if (semNum == 1) {
                    Console.WriteLine("\n*************************************************************");
                    Console.WriteLine($"  {name}, you can award an amount of ${totalScholarShip} scholarship ");
                    Console.WriteLine("*************************************************************");
                } else {
                    Console.WriteLine("\n*************************************************************");
                    Console.WriteLine($"*\t{name}, you can award                            *");
                    Console.WriteLine("*\tGPA -\t\tScholarship                         *");
                    Console.WriteLine($"*\t>= {GPA}\t\t  ${totalScholarShip}                          *\n*\t< {GPA}\t\t  $0.0                              *");
                    Console.WriteLine("*************************************************************");
                }
                Console.WriteLine("Do you want to check with another semester? (y/n)");
                String checker = Console.ReadLine();
                if (checker.Equals("n")) {
                    counter = false;
                }
            } while(counter);
            Console.WriteLine($"Thank you!, {name}");
        }
    }
}
